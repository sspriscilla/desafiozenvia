# Configure the AWS Provider
provider "aws" { 
	region  = "us-east-2"
	shared_credentials_file = "/Users/priscilladossantos/scriptsterraform/.aws/credentials"
	profile = "awsterraform"
}


resource "aws_instance" "kube1" {
	ami = "ami-04781752c9b20ea41"
	instance_type = "t3a.small"
	subnet_id = "subnet-e2e02c89"
        key_name   = "keypairdesafiozenvia"
        tags = {Name="KUBE1"}
}
resource "aws_instance" "kube2" {
        ami = "ami-04781752c9b20ea41"
        instance_type = "t3a.small"
        subnet_id = "subnet-e2e02c89"
        key_name   = "keypairdesafiozenvia"
        tags = {Name="KUBE2"}
}
resource "aws_instance" "kube3" {
        ami = "ami-04781752c9b20ea41"
        instance_type = "t3a.small"
        subnet_id = "subnet-e2e02c89"
        key_name   = "keypairdesafiozenvia"
        tags = {Name="KUBE3"}
}
